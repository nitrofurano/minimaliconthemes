title: Puredyne Minimal
author: Paulo Silva
based on work from: Ulisse Perusin, Lapo Calamandrei, Jakub Steiner, Andreas Nilssen - iconset from UbuntuStudio 8.04
licence: Creative Commons - NonCommercial-ShareAlike - http://creativecommons.org/licenses/by-sa/2.0/
colour used: #D62839

# minimaliconthemes

colours used on icon themes:
- MintMinimal: #77CC33
- BunsenLabsMinimal: #B2B2B2
- UbuntuStudioMinimal: #009FF9
- PuredyneMinimalAlternate2: #7F7F7F
- PuredyneMinimalAlternate1: #AB545C
- PuredyneMinimal: #D62839
- HumanMinimal: #F57900
